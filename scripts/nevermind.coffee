# Description:
#   Doesn't mind.
#
# Dependencies:
#   None
#
# Configuration:
#   None
#
# Commands:
#   hubot <nevermind|nm> - Doesn't mind.
#
# Author:
#   sils

module.exports = (robot) ->
  robot.respond /(nevermind|nm)$/i, (msg) ->
    msg.send "I'm sorry :("
